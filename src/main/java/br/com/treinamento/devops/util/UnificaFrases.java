package br.com.treinamento.devops.util;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class UnificaFrases {
	
	
	private static final String PATH =  Paths.get("").toAbsolutePath().toString()
			+ "\\src\\test\\resources\\features\\";

	public static void main(String[] args) {
		Set<String> setPalavrasGherking = new HashSet<String>();
		setPalavrasGherking.add("E ");
		setPalavrasGherking.add("Entao ");
		setPalavrasGherking.add("Quando ");
		
		Scanner scanner;
		
		String feature = "geral.txt";
		
		Set<String> setFrases = new TreeSet<String>();
		try {
			scanner = new Scanner(new FileReader(PATH + feature)).useDelimiter("\\n");
			while (scanner.hasNext()) {
				String linha = scanner.next();
				/*for (Iterator iterator = setPalavrasGherking.iterator(); iterator.hasNext();) {
					String palavraGherking = (String) iterator.next();
					if(linha.contains(palavraGherking)) {
						setFrases.add(linha.trim());
					}
				}*/
				
				for (Iterator iterator = setPalavrasGherking.iterator(); iterator.hasNext();) {
					String palavraGherking = (String) iterator.next();
					setFrases.add(palavraGherking+linha);					
				}
			}
			System.out.println("iniciooo.....");
			for (Iterator iterator = setFrases.iterator(); iterator.hasNext();) {
				String frase = (String) iterator.next();
				System.out.print(frase);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
