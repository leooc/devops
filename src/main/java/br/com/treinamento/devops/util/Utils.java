package br.com.treinamento.devops.util;

import com.google.gson.JsonElement;

/**
 * Name: {@link Utils}
 * 
 * Prop�sito: Classe, com fun��es utilit�rias
 * 
 * @see
 * 
 * @version 1.0
 *
 */
public final class Utils {
	
	/**
	 * Formatar string (mask currency) para double
	 * @param value
	 * @return
	 */
	public static double formatPreco(String value) {
		if (value.isEmpty())
			return 0D;
		return Double.valueOf(formatValue(value));
	}
	
	/**
	 * Formata valor, que usa m�scara monet�ria
	 * @param value
	 * @return
	 */
	private static String formatValue(String value) {
		String result = value.replaceAll("\\s*R\\$\\s*", "").replaceAll("\\.", "").replaceAll(",", "\\.");
		return result;
	}
	
	public static boolean isNull(Object obj) {
		return obj == null;
	}
	
	/**
	 * Verifica se o campo � null ou vazio
	 * @param param
	 * @return
	 */
	public static boolean isEmptyOrNull(String param) {
		return param != null && param.isEmpty();
	}
		
	/**
	 * Verifica se o campo n�o � null ou vazio
	 * @param param
	 * @return
	 */
	public static boolean isNotEmptyOrNull(String param) {
		return param != null && !param.isEmpty();
	}
	
	/**
	 * Este m�todo valida apenas a empresa adquirente
	 * @param param
	 * @return
	 */
	public static boolean validarApenasEmpresaAdq(String param) {
		if (param.equals("") || param.equals("001") || param.equals("002")) {
			return true;
		}
		return false;
	}
	
	/**
	 * Obter o valor do campo sem espa�o
	 * @param element
	 * @return
	 */
	public static String getValue(JsonElement element) {
		return element.getAsString().trim();
	}
	
	/**
	 * Converte o valor de string para long
	 * @param value
	 * @return
	 */
	public static long tratarParametroLong(String value) {
		if (!value.isEmpty()) return Long.parseLong(value);
		return 0L;
	}
	
	/**
	 * Converte o valor de string para int
	 * @param value
	 * @return
	 */
	public static int tratarParametroInt(String value) {
		if (!value.isEmpty()) return Integer.parseInt(value);
		return 0;
	}
	
	/**
	 * Converte o valor de string para double
	 * @param value
	 * @return
	 */
	public static double tratarParametroDouble(String value) {
		if (!value.isEmpty()) return Double.parseDouble(value);
		return 0D;
	}
	
	public static String isEmpty(String param) {
		
		if (!param.isEmpty()) return param;
		return "" ;
	}
	
	/**
	 * Valida se o Nome do Scenario possui mais de 255 caracteres, 
	 * 	se for maior ele limita ao maximo de 255 caracteres descartando o final do nome
	 * 
	 * @param scenarioName : Nome do cenario que est� sendo utilizado
	 * @return <I> String </I>: nome do cenario limitado a 255 caracteres
	 * 
	 * 
	 */
	public static String validateName(String scenarioName) {
		if (scenarioName.length() < 255) {
			return scenarioName;
		} else {
			return scenarioName.substring(0, 254);
		}
	}
	
}
