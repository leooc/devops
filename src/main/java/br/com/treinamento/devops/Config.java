package br.com.treinamento.devops;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Configuration;

import br.com.treinamento.devops.selenium.browser.BrowserType;
import br.com.treinamento.devops.selenium.browser.WebDriverFactory;
import br.com.treinamento.devops.selenium.command.Command;
import br.com.treinamento.devops.selenium.command.CommandFactory;
import br.com.treinamento.devops.selenium.command.CommandType;
import br.com.treinamento.devops.selenium.searchelement.SearchElement;
import br.com.treinamento.devops.selenium.searchelement.SearchElementFactory;
import br.com.treinamento.devops.selenium.searchelement.SearchElementType;


@Configuration
public class Config {
	
	private WebDriver webDriver;
	private SearchElement searchElement;
	private Command command;
	private String scenario;
	private Map<String,Map<String, Object>> mapCenarios = new HashMap<String,Map<String, Object>>();
	
	/**
	 * Obter instância de webDriver
	 * @return
	 * @throws Exception
	 */
	public WebDriver getWebDriver(BrowserType type, String address) throws Exception {
		webDriver = WebDriverFactory.getWebDriver(type, address);
		webDriver.switchTo();
		return webDriver;
	}
	
	public Command getCommand(CommandType type) {
		command = CommandFactory.getCommand(type, webDriver);
		return command;
	}
	
	/**
	 * @return the webDriver
	 */
	public WebDriver getWebDriver() {
		return webDriver;
	}
	/**
	 * @param webDriver the webDriver to set
	 */
	public void setWebDriver(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	/**
	 * @return the mapCenarios
	 */
	public Map<String, Map<String, Object>> getMapCenarios() {
		return mapCenarios;
	}
	/**
	 * @param mapCenarios the mapCenarios to set
	 */
	public void setMapCenarios(Map<String, Map<String, Object>> mapCenarios) {
		this.mapCenarios = mapCenarios;
	}
	
	public SearchElement getSearchElement(SearchElementType type) {
		searchElement = SearchElementFactory.getSearchElement(type, webDriver);
		return searchElement;
	}

	/**
	 * @return the searchElement
	 */
	public SearchElement getSearchElement() {
		return searchElement;
	}

	/**
	 * @param searchElement the searchElement to set
	 */
	public void setSearchElement(SearchElement searchElement) {
		this.searchElement = searchElement;
	}

	/**
	 * @return the command
	 */
	public Command getCommand() {
		return command;
	}

	/**
	 * @param command the command to set
	 */
	public void setCommand(Command command) {
		this.command = command;
	}

	/**
	 * @return the scenario
	 */
	public String getScenario() {
		return scenario;
	}

	/**
	 * @param scenario the scenario to set
	 */
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	public Map<String, Object> getMapCenario(String scenario) {
		return mapCenarios.get(scenario);
	}
	
}
