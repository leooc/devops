package br.com.treinamento.devops.vo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TrafegoDadosVO {
	
	private String[] produtos;
	private List<String> lstProdutos = new ArrayList<String>();
	private String tamanhoLista;
	

	/**
	 * @return the tamanhoLista
	 */
	public String getTamanhoLista() {
		return tamanhoLista;
	}

	/**
	 * @param tamanhoLista the tamanhoLista to set
	 */
	public void setTamanhoLista(String tamanhoLista) {
		this.tamanhoLista = tamanhoLista;
	}

	/**
	 * @return the produtos
	 */
	public String[] getProdutos() {
		return produtos;
	}

	/**
	 * @param produtos the produtos to set
	 */
	public void setProdutos(String[] produtos) {
		this.produtos = produtos;
	}

	/**
	 * @return the lstProdutos
	 */
	public List<String> getLstProdutos() {
		return lstProdutos;
	}

	/**
	 * @param lstProdutos the lstProdutos to set
	 */
	public void setLstProdutos(List<String> lstProdutos) {
		this.lstProdutos = lstProdutos;
	}

}
