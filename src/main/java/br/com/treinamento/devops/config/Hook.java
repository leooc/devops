package br.com.treinamento.devops.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.treinamento.devops.Config;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hook {
	
	@Autowired
	private Config config;

	@Before("@before")
	public void before(Scenario scenario) throws Exception {
		config.setScenario(scenario.getName());
		Map<String, Object> mapAtributosCenario = new HashMap<String, Object>();
		config.getMapCenarios().put(scenario.getName(), mapAtributosCenario);

	}

	@After("@after")
	public void closeBrowser(Scenario scenario) throws Exception {
		boolean isPassed = !scenario.isFailed();

		if (config.getWebDriver() != null) {
			try {
//				config.getWebDriver().close();
//				config.getWebDriver().quit();
			} catch (Exception e) {
				throw e;
			} finally {
				try {
					//caso ocorra exception dentro do alm matar instancia
					if (isPassed) {
//						alm.upload(isPassed);
					}
				} catch (Exception e) {
					throw e;
				} finally {
					config.setWebDriver(null);
				}
				config.setWebDriver(null);
			}
		}

	}

}
