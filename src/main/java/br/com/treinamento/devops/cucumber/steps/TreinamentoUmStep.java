package br.com.treinamento.devops.cucumber.steps;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.treinamento.devops.Config;
import br.com.treinamento.devops.selenium.command.Command;
import br.com.treinamento.devops.selenium.searchelement.SearchElement;
import br.com.treinamento.devops.vo.TrafegoDadosVO;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;


public class TreinamentoUmStep {
	
	@Autowired
	private Config config;
	private WebDriver webDriver;
	private SearchElement searchElement;
	private Command command;
	
	@Autowired
	private TrafegoDadosVO trafegoDadosVO;
	
	final static Logger logger = Logger.getLogger(TreinamentoUmStep.class);
	
	private static final String XPATH_ENDERECO_CIELO = "https://formulario.cielo.com.br/form2/passo-1?solucao=12";
	private static final String XPATH_CAMPO_NOME = "//input[@placeholder='Digite o nome completo']";
	private static final String XPATH_NUMERO_TELEFONE = "//input[@name='contact_phone']";
	private static final String XPATH_CAMPO_EMAIL = "//input[@placeholder='Digite o e-mail do proprietário']";
	private static final String XPATH_CONFIRMA_EMAIL = "//input[@placeholder='Confirme o e-mail do proprietário']";
	private static final String XPATH_BOTAO_CONTINUAR = "//button[contains(text(),'Continuar')]";
	private static final String XPATH_LABEL_PAGINA_RESULT = "//h2[contains(text(),'Informações adicionais')]";
	
	@Dado("^entro no \"([^\"]*)\" da cielo$")
	public void entro_no_da_cielo(String site) throws Throwable {
		webDriver = config.getWebDriver();
		webDriver.get(XPATH_ENDERECO_CIELO);
	}

	@Dado("^preencho os campos do formulario$")
	public void preencho_os_campos_do_formulario() throws Throwable {
		
		searchElement = config.getSearchElement();
		command = config.getCommand();
		if(searchElement.exists(XPATH_CAMPO_NOME, "CAMPO: nome")){
			WebElement campoNome = searchElement.find(XPATH_CAMPO_NOME, "campo nome");
			command.send(campoNome, "Leonardo Costa");
		}
		
		if(searchElement.exists(XPATH_NUMERO_TELEFONE, "Campo: telefone")) {
			WebElement campoTelefone = searchElement.find(XPATH_NUMERO_TELEFONE, "Campo: telefone");
			command.send(campoTelefone, "11944446666");
		}
		
		if(searchElement.exists(XPATH_CAMPO_EMAIL, "Campo: email")) {
			WebElement campoEmail = searchElement.find(XPATH_CAMPO_EMAIL, "Campo: email");
			command.send(campoEmail, "leo@leo.com");
		}
		
		if(searchElement.exists(XPATH_CONFIRMA_EMAIL, "Campo: confirma email")) {
			WebElement campoConfirmaEmail = searchElement.find(XPATH_CONFIRMA_EMAIL, "Campo: confirma email");
			command.send(campoConfirmaEmail, "leo@leo.com");
		}
		
	}

	@Dado("^realizo o click para continuar$")
	public void realizo_o_click_para_continuar() throws Throwable {
		
		if(searchElement.exists(XPATH_BOTAO_CONTINUAR, "Campo: BOTAO CONTINUAR")) {
			WebElement botaoContinuar = searchElement.find(XPATH_BOTAO_CONTINUAR, "Campo: botao continuar");
			command.click(botaoContinuar);
		}
	}

	@Entao("^verifico se apareceu o label \"([^\"]*)\"$")
	public void verifico_se_apareceu_o_label(String arg1) throws Throwable {
		
		if(searchElement.exists(XPATH_LABEL_PAGINA_RESULT, "label outra tela")) {
			logger.info("Sucesso !");
			System.out.println("Sucesso !");
		}
	}
	

	
	
}