package br.com.treinamento.devops.cucumber;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.treinamento.devops.Config;
import br.com.treinamento.devops.config.InitializeConfig;
import br.com.treinamento.devops.selenium.browser.BrowserType;
import br.com.treinamento.devops.selenium.command.Command;
import br.com.treinamento.devops.selenium.command.CommandType;
import br.com.treinamento.devops.selenium.searchelement.SearchElement;
import br.com.treinamento.devops.selenium.searchelement.SearchElementType;
import br.com.treinamento.devops.util.Constants;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;

@InitializeConfig
public class InitializeTest {
	
	@Autowired
	private Config config;
	private WebDriver webDriver;
	private SearchElement searchElementGeneric;
	private Command command;
	private String path = Paths.get("").toAbsolutePath().toString() + "\\resource\\files\\contador.txt";
	private File file = new File(path);
	private PrintWriter printWriter;
	private BufferedReader br;
	private String line, browser;
	
	@Before
	public void setup() throws Exception {

		PropertyConfigurator.configure(Paths.get("").toAbsolutePath().toString() + "/log4j.properties");

		if (file.getAbsoluteFile().exists()) {
			FileReader fileReader = new FileReader(file);
			BufferedReader br = new BufferedReader(fileReader);
			if (!br.readLine().isEmpty()) {
				fileReader.close();
				br.close();
				file.delete();
			}
		}
	}
	
	@Dado("^que o navegador esteja aberto$")
	public void que_o_navegador_esteja_aberto(DataTable arg1) throws Throwable {
		FileReader fileReader = null;
		List<List<String>> data = arg1.raw();
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			try {
				printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file.getAbsoluteFile(), true)));
				printWriter.print("X");
				printWriter.flush();
			} catch (IOException e) {
				e.getMessage();
			}
			fileReader = new FileReader(file);
			br = new BufferedReader(fileReader);
			line = br.readLine();
			int nroExec = line.length();
			browser = data.get(nroExec - 1).get(0);
		} catch (Exception e) {
			e.getMessage();
		}
		switch (browser) {
		case "Chrome":
			webDriver = config.getWebDriver(BrowserType.chrome, "Localhost");
			System.out.println("Chrome iniciado");
			break;
		case "Firefox":
			webDriver = config.getWebDriver(BrowserType.firefox, "Localhost");
			System.out.println("FF iniciado");
			break;
		case "IE":
			webDriver = config.getWebDriver(BrowserType.internetExplorer, "Localhost");
			System.out.println("IE iniciado");
			break;
		default:
			break;
		}
		
		config.setWebDriver(webDriver);
		searchElementGeneric = config.getSearchElement(SearchElementType.GENERIC);
		config.setSearchElement(searchElementGeneric);
		command = config.getCommand(CommandType.GENERIC);
		config.setCommand(command);
				
	}

}