package br.com.treinamento.devops.selenium.command;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import br.com.treinamento.devops.selenium.command.exception.CommandException;
import br.com.treinamento.devops.selenium.searchelement.SearchElement;
import br.com.treinamento.devops.selenium.searchelement.SearchElementFactory;
import br.com.treinamento.devops.selenium.searchelement.SearchElementType;

public class CommandGeneric extends Command{
	
	/**
	 * Construtor
	 * 
	 * @param webDriver
	 */
	public CommandGeneric(WebDriver webDriver) {
		super(webDriver);
	}

	@Override
	public void navigateMenu(String newNavegation) throws Exception {
		SearchElement searchElement = SearchElementFactory.getSearchElement(SearchElementType.GENERIC, webDriver);
		if (newNavegation.equals("gerenciar meu negócio:Dados Cadastrais")) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			click(searchElement.find("//span[contains(@class,'navEstb')]", "Icone dados cadastrais"));
			return;
		}
		String xpath = null, xpath2 = null;
		String[] Navigation = newNavegation.split(":");
		for (int i = 0; i < Navigation.length; i++) {
			if(i == 0){
				xpath = "//div[contains(text(),'nomeDoMenu')]/..";
			}else if(i == 1){
				xpath = "//ul[@class='multi-column-dropdown']//li//a[contains(text(),'nomeDoMenu')]";
				if(Navigation.length > 2){
					xpath = xpath.replace("nomeDoMenu", Navigation[i].toString());
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
					}
					WebElement itemMenu = searchElement.find(xpath, "Item menu");
					Actions action = new Actions(webDriver);
			        action.moveToElement(itemMenu).build().perform();
			        continue;
				}
			}else if(i == 2){
				xpath = "//a[@class='ng-binding ng-scope' and contains(text(),'nomeDoMenu')]";
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
			}
			xpath = xpath.replace("nomeDoMenu", Navigation[i].toString());
			WebElement itemMenu = searchElement.find(xpath, "Item menu");
			click(itemMenu);
			itemMenu = null;
		}
	}

	@Override
	public String getText(WebElement webElement) {
		return webElement.getText();
	}

	@Override
	/**
	 * Simular um click
	 * 
	 * @param webElement
	 */
	public void click(WebElement webElement) throws Exception {
		try {
			webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
			webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));

			if (webDriver.toString().toLowerCase().contains("internet explorer")) {
				((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", webElement);
			} else {
				webElement.click();
			}

			logger.debug("Elemento: '" + webElement.toString() + "' realizado ação 'click'");
		} catch (Exception e) {
			msgError = "Elemento: '" + webElement.toString() + "' não realizado ação 'click'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

}
