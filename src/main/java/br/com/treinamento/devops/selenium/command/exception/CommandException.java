package br.com.treinamento.devops.selenium.command.exception;

public class CommandException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandException(String mensagem) {
		super(mensagem);
	}
	
	public CommandException(Exception e) {
		super(e);
	}
	
	public CommandException(String mensagem, Exception e) {
		super(mensagem, e);
	}

}
