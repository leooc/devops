package br.com.treinamento.devops.selenium.browser;

import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import br.com.treinamento.devops.util.Constants;

public class WebDriverFactory {
	
	private final static String INTERNET_EXPLORER = "internetexplorer";
	
	private final static String FIREFOX = "firefox";
	
	private final static String CHROME = "chrome";
	
	private final static String EDGE = "edge";
	
		/**
		 * Obter o web driver (IE, Firefox, Chrome, PhantomJS)
		 * @param browserType
		 * @param addressSelenium
		 * @return
		 * @throws Exception
		 */
		public static WebDriver getWebDriver(BrowserType browserType, String addressSelenium ) throws Exception {
			return getWebDriver(browserType.toString(), addressSelenium);
		}
		
		/**
		 * Obter tipo do browser informado pelo usuário
		 * @param BrowserType
		 * @param addressSelenium
		 * @return
		 * @throws Exception
		 */
		public static WebDriver getWebDriver(String browserType, String addressSelenium ) throws Exception {
			
			switch (browserType.toLowerCase()) {
		    	case INTERNET_EXPLORER:
		    		return getInternetExplorerDriver(); 
			 	case FIREFOX:
			    	return getFirefoxDriver();    		    
			    case CHROME:
			    	return getChromeDriver(); 
			    case EDGE:
//			    	return getEdgeDriver();
			    default:
			    	String e = "Não foi informado browser suportado pela aplicação";
					throw new Exception(e);
			}
		}
		
		/**
		 * Obter driver do Chrome
		 * @return webDriver do Chrome
		 */
		private static WebDriver getChromeDriver() {
			System.setProperty("webdriver.chrome.driver", Paths.get(Constants.VAZIO).toAbsolutePath().toString()+ "\\resource\\drivers\\chromedriver.exe");
			WebDriver webDriver = new ChromeDriver(getCapabilitiesChromeDriver());
			webDriver.manage().window().maximize();
			return webDriver;
		}
		
		/**
		 * Obter driver do Firefox
		 * @return webDriver do Firefox
		 */
		private static WebDriver getFirefoxDriver() {
			System.setProperty("webdriver.gecko.driver", Paths.get(Constants.VAZIO).toAbsolutePath().toString()+ "\\resource\\drivers\\geckodriver.exe");
			WebDriver webDriver = new FirefoxDriver();
			return webDriver;
		}
		
		/**
		 * Obter driver do Internet Explorer
		 * @return
		 */
		private static WebDriver getInternetExplorerDriver() {
			System.setProperty("webdriver.ie.driver", Paths.get(Constants.VAZIO).toAbsolutePath().toString()+ "\\resource\\drivers\\IEDriverServer.exe");
			WebDriver webDriver = new InternetExplorerDriver(getCapabilitiesInternetExplorerDriver());
			webDriver.manage().deleteAllCookies();
			webDriver.manage().window().maximize();
			return webDriver;
		}
		
		/**
		 * Obter capabilities para o internet explorer
		 * @return
		 */
		private static DesiredCapabilities getCapabilitiesInternetExplorerDriver() {
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");
			capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true); 
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability("javascriptEnabled", true);
			return capabilities;
		}
		
		private static DesiredCapabilities getCapabilitiesChromeDriver() {
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();

			Map<String, Object> preferences = new Hashtable<String, Object>();
			preferences.put("profile.default_content_settings.popups", 0);
			preferences.put("download.prompt_for_download", "false");
			preferences.put("download.default_directory", Paths.get("").toAbsolutePath().toString() + "/resource/comparison");

			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", preferences);

			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			return capabilities;
		}

}
