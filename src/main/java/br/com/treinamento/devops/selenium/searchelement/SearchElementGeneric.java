package br.com.treinamento.devops.selenium.searchelement;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.treinamento.devops.selenium.searchelement.exception.SearchElementException;
import junit.framework.Assert;

public class SearchElementGeneric extends SearchElement {
	
	/**
	 * Construtor
	 * 
	 * @param webDriver
	 */
	public SearchElementGeneric(WebDriver webDriver) {
		super(webDriver);
		this.webDriverWait = new WebDriverWait(this.webDriver, TIME_OUT);
	}

	private WebDriverWait webDriverWait;

	private static final Logger logger = Logger.getLogger(SearchElementGeneric.class);

	@Override
	public WebElement find(String xpath, String friendly) throws Exception {

		int count = 0;
		int maxTries = 60;
		boolean down = false;
		String JAVA_SCRIPT = "arguments[0].focus();arguments[0].scrollIntoView(true);";
		while (true) {
			try {
				this.webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
				WebElement webElement = webDriver.findElement(By.xpath(xpath));
				if (webDriver.toString().toLowerCase().contains("firefox")) {
					WebElement webElementScroll = webDriver.findElement(By.xpath(xpath + "/../.."));
					JavascriptExecutor executor = (JavascriptExecutor) webDriver;
					Long value = (Long) executor.executeScript("return window.pageYOffset;");

					Point location = webElementScroll.getLocation();
					location.y = location.y - value.intValue();
					down = (location.y > value);

					((JavascriptExecutor) webDriver).executeScript(JAVA_SCRIPT, webElement);
					((JavascriptExecutor) webDriver)
							.executeScript("window.scrollBy(0, " + (down ? "-250" : "250") + ");");
					logger.info("Elemento: '" + friendly + "' localizado pelo xpath: '" + xpath + "'");
				} else {
					((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", webElement);
					((JavascriptExecutor) webDriver).executeScript("scroll(0, -250);");
					logger.info("Elemento: '" + friendly + "' localizado pelo xpath: '" + xpath + "'");
				}
				return webElement;
			} catch (NoSuchElementException e) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				}
				if (++count == maxTries) {
					String errMsg = "Elemento: '" + friendly + "' não localizado pelo xpath: '" + xpath
							+ "', tentativas: " + count;
					logger.fatal(errMsg, e);
					throw new SearchElementException(errMsg);
				}
			}
		}
	}

	@Override
	public List<WebElement> finds(String xpath, String friendly) throws Exception {

		int count = 0;
		int maxTries = 60;
		while (true) {
			try {
				waitAngularJS();
				// this.webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));

				List<WebElement> webElements = webDriver.findElements(By.xpath(xpath));
				logger.info("Lista de Elementos: '" + friendly + "' localizados pelo xpath: '" + xpath + "'");
				return webElements;
			} catch (NoSuchElementException e) {
				if (++count == maxTries) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
					}
					String errMsg = "Lista de Elementos: '" + friendly + "' não localizados pelo xpath: '" + xpath
							+ "'";
					logger.fatal(errMsg, e);
					throw new SearchElementException(errMsg);
				}
			}
		}

	}

	private void waitAngularJS() {
		
		JavascriptExecutor js = (JavascriptExecutor) webDriver;
		for (int i = 0; i < (TIME_OUT * 20); i++) {
			if (i == TIME_OUT) {
				Assert.fail("TIMEOUT - carregamento dos objetos");
			}
			try {
				Thread.sleep(800);
			} catch (InterruptedException e) {
			}
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}

	}
	
	public Boolean exists(String Xpath, String friendly) throws Exception {
		return exists(Xpath, friendly, TIME_OUT);
	}
	
	/**
	 * Metodo para verificar se um WebElement existe na pagina em exibição,
	 * permitindo que seja informado um tempo de espera para aguardar a
	 * existencia do elemento.
	 * 
	 * @param xpath
	 *            - informe o xpath de localização do objeto
	 * @param friendly
	 *            - informe o nome amigavel para exibição
	 * @param timeWait
	 *            - informe o tempo que o sistema deve aguardar em segundos o
	 *            objeto existir na tela.
	 * @return true - caso localize o objeto | False caso não localize o objeto
	 * @throws Exception
	 */
	public Boolean exists(String xpath, String friendly, long timeWait) {
		try {

			waitAngularJS();

			WebDriverWait tempWebDriverWait = new WebDriverWait(webDriver, timeWait);
			tempWebDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			logger.info("Elemento: '" + friendly + "' existe com xpath: '" + xpath + "'");
			return true;
		} catch (TimeoutException e) {
			logger.info("Elemento: '" + friendly + "' não existe com xpath: '" + xpath + "'");
			return false;
		}
	}

}
