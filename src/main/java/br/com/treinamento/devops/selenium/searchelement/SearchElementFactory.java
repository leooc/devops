package br.com.treinamento.devops.selenium.searchelement;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import br.com.treinamento.devops.selenium.searchelement.exception.SearchElementException;

public class SearchElementFactory {

	
	/**
	 * Construtor privado. Classe não deve ser instanciada.
	 */
	private SearchElementFactory() {
	}

	/**
	 * Devolve uma nova instancia de SearchElement com a implementação
	 * correspondente ao SearchElementType passado. Para passar o type desejado
	 * deve usar uma das opções do enum SearchElementType.
	 * 
	 * @param type
	 *            opção disponível no enum SearchElementType indicando qual a
	 *            implementação desejada.
	 * @param webDriver
	 *            WebDriver que a SearchElement usará.
	 * @return Retorna uma implementação da SearchElement de acordo com o type
	 *         selecionado.
	 */
	public static SearchElement getSearchElement(SearchElementType type, WebDriver webDriver) {
		return type.getSearchElement(webDriver);
	}

	/**
	 * Devolve uma nova instancia de SearchElement com a implementação
	 * correspondente à String passada.
	 * 
	 * @param type
	 *            String correspondente à uma opção implementada.
	 * @param webDriver
	 *            WebDriver que a SearchElement usará.
	 * @return Retorna uma implementação da SearchElement de acordo com o type
	 *         selecionado.
	 * @throws Exception 
	 * @throws NoSuchElementException
	 *             Quando a opção informada não é válida.
	 */
	public static SearchElement getSearchElement(String searchElementType, WebDriver webDriver) throws Exception {
		SearchElementType[] values = SearchElementType.values();
		for (int i = 0; i < values.length; i++) {
			if (searchElementType.equalsIgnoreCase(values[i].toString().trim())) {
				return getSearchElement(values[i], webDriver);
			}
		}
		throw new SearchElementException(
				"Não foi possível instanciar nenhum SearchElement do tipo " + searchElementType);
	}
	
}
