package br.com.treinamento.devops.selenium.searchelement;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class SearchElement {
	
	final static long TIME_OUT = 60;
	
	protected WebDriver webDriver;	
	protected WebDriverWait webDriverWait;
	
	/**
	 * Construtor
	 * 
	 * @param webDriver
	 */
	public SearchElement(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.webDriverWait = new WebDriverWait(this.webDriver, TIME_OUT);
	}
	
	/**
	 * Busca um WebElement a partir de um xpath.
	 * 
	 * @param xpath
	 *            Informe o xpath de localização do objeto
	 * @param friendly
	 *            - informe o nome amigavel para exibição
	 * @return Retorna o WebElement encontrado pelo xpath
	 * @author Leonardo Costa
	 * @throws Exception 
	 * @throws NoSuchElement
	 *             caso não seja encontrado nenhum elemento pelo xpath passado
	 */
	public abstract WebElement find(String xpath, String friendly) throws Exception;
	
	/**
	 * Busca elementos a partir de um xpath e retorna todos que foram
	 * encontrados
	 * 
	 * @param xpath
	 *            - informe o xpath de localização dos objetos
	 * @param friendly
	 *            - informe o nome amigavel para exibição
	 * @author Leonardo  Costa
	 * @return Retorna lista de objetos no encontrado pelo xpath
	 * @throws Exception 
	 */
	public abstract List<WebElement> finds(String xpath, String friendly) throws Exception;
	
	/**
	 * @param value
	 *            - informe o xpath de localização do objeto
	 * @param friendly
	 *            - informe o nome amigavel para exibição
	 * @return true - caso localize o objeto | False caso não localize o objeto
	 * @author Leonardo  Costa
	 * @throws Exception
	 */
	public Boolean exists(String Xpath, String friendly) throws Exception {
		return exists(Xpath, friendly, TIME_OUT);
	}
	
	/**
	 * Metodo para verificar se um WebElement existe na pagina em exibição,
	 * permitindo que seja informado um tempo de espera para aguardar a
	 * existencia do elemento.
	 * 
	 * @param xpath
	 *            - informe o xpath de localização do objeto
	 * @param friendly
	 *            - informe o nome amigavel para exibição
	 * @param timeWait
	 *            - informe o tempo que o sistema deve aguardar em segundos o
	 *            objeto existir na tela.
	 * @return true - caso localize o objeto | False caso não localize o objeto
	 * @author Leonardo Costa
	 * @throws Exception
	 */
	public Boolean exists(String xpath, String friendly, long timeWait) {
		try {
			JavascriptExecutor js = (JavascriptExecutor)webDriver;
			for (int i=0; i<TIME_OUT*10; i++)
		    { 
		            Thread.sleep(10);
		        if (js.executeScript("return document.readyState").toString().equals("complete"))
		        { 
		            break; 
		        }   
		      }
			webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			WebDriverWait tempWebDriverWait = new WebDriverWait(webDriver, timeWait);
			tempWebDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
//			logger.debug("Elemento: '" + friendly + "' localizado pelo xpath: '" + xpath + "'");
			return true;
		} catch (Exception e) {
//			logger.debug("Elemento: '" + friendly + "' não localizado pelo xpath: '" + xpath + "'",e);
			return false;
		}
	}

}
