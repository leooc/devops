package br.com.treinamento.devops.selenium.command;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;

import br.com.treinamento.devops.selenium.command.exception.CommandException;


/**
 * 
 * Name: {@link CommandFactory}
 * 
 * Propósito: Fábrica de objetos - CommandFactory
 * 
 * @see
 * 
 * @version 1.0
 *
 */
public class CommandFactory {
	
	private CommandFactory() {
		
	}
	
	/**
	 * Cria uma instância de Command de acordo com a opção selecionada do Enum CommandType.
	 * 
	 * @param commandType Qual o tipo de Command deve ser instanciada.
	 * @param webDriver WebDriver que será utilizado
	 * @return Retorna uma nova instancia de Command
	 */
	public static Command getCommand(CommandType commandType, WebDriver webDriver){
		return commandType.getCommand(webDriver);
	}
	
	/**
	 * Cria uma instância de Command de acordo com a opção passada por parâmetro.
	 * 
	 * @param commandType Qual tipo de Command deve ser instanciada.
	 * @param webDriver WebDriver que será utilizado
	 * @return Retorna uma nova instancia de Command
	 * @throws Exception 
	 * @throws NoSuchElementException caso a opção informada não seja uma opção válida
	 */
	public static Command getCommand (String commandType, WebDriver webDriver) throws Exception {
		CommandType [] values = CommandType.values();
		for (int i = 0 ; i< values.length ; i++) {
			if (commandType.equalsIgnoreCase(values[i].toString().trim())) {
				return getCommand (values[i], webDriver);				
			}
		}
		throw new CommandException ("Não foi possível instanciar nenhuma Command do tipo " + commandType);
 	}
}
