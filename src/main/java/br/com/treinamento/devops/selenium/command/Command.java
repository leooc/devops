package br.com.treinamento.devops.selenium.command;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import br.com.treinamento.devops.selenium.command.exception.CommandException;

public abstract class Command {

	final static Logger logger = Logger.getLogger(Command.class);

	private final long TIME_OUT;

	private final String FOCUS_SCRIPT = "arguments[0].focus(); arguments[0].scrollIntoView(true);";

	protected String msgError;

	protected WebDriver webDriver;

	protected WebDriverWait webDriverWait;

	/**
	 * Construtor
	 * 
	 * @param webDriver
	 */
	public Command(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.TIME_OUT = 30;
		this.webDriverWait = new WebDriverWait(this.webDriver, TIME_OUT);
	}

	/**
	 * Clicar em qualquer componente de 'Aba'
	 * 
	 * @param nameTab
	 * @throws Exception
	 */
	public void clickOnTab(String nameTab) {
	}
	
	/** carrega o nome do texto no campo
	 * @param webElement
	 * @return
	 */
	public String getText(WebElement webElement) {
		return null;
	}

	/**
	 * Selecionar item dentro do combo
	 * 
	 * @param webElement
	 * @param value
	 * @throws Exception 
	 */
	public void selectComboItem(WebElement webElement, String value) throws Exception {
		try {
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
			focus(webElement);
			if (webDriver.toString().toLowerCase().contains("internet explorer")){
				((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", webElement);
			} else {
				webElement.click();
			}
			String xpath = "//div[contains(@style,'display: block')]//descendant::li[normalize-space(@data-label)='"
					+ value + "']";
			WebElement comboOption = webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", comboOption);
			logger.debug("Elemento: '" + comboOption.toString() + "' realizado ação 'click' no valor '" + value + "'");
		} catch (TimeoutException e) {
			msgError = "Erro TimeOut após aguardar " + TIME_OUT + " segundos, o elemento: '" + webElement.toString()
					+ "' não realizado ação 'click' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		} catch (StaleElementReferenceException e) {
			msgError = "Referencia do elemento: '" + webElement.toString()
					+ "' alterado durante a execução da ação 'click' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}
	
	/**
	 * Seleciona item no combo de assunto na tela de ouvidoria
	 * @param webElement
	 * @param value
	 * @throws Exception 
	 */
	public void selectComboAssuntoOuvidoria(WebElement webElement, String value) throws Exception{
		try{
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
			if (webDriver.toString().toLowerCase().contains("internet explorer")){
				((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", webElement);
			} else {
				webElement.click();
			}
			/*//String xpath = "//div[contains(@style,'display: block')]//descendant::li[normalize-space(@data-label)='"
					+ value + "']";*/
			String xpath = "//div[contains(@selected,'selected')]//descendant::li[normalize-space('"+ value +"')]//following::a[contains(text(),'"+ value +"')]";
			
			WebElement comboOption = webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", comboOption);
			logger.debug("Elemento: '" + comboOption.toString() + "' realizado ação 'click' no valor '" + value + "'");
		} catch (TimeoutException e) {
			msgError = "Erro TimeOut após aguardar " + TIME_OUT + " segundos, o elemento: '" + webElement.toString()
					+ "' não realizado ação 'click' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		} catch (StaleElementReferenceException e) {
			msgError = "Referencia do elemento: '" + webElement.toString()
					+ "' alterado durante a execução da ação 'click' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Enviar valor para os campos de 'input'
	 * 
	 * @param webElement
	 * @param value
	 * @throws Exception 
	 */
	public void send(WebElement webElement, String value) throws Exception {
		
		try {
			webElement = webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
			webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			if (webElement.getAttribute("class").contains("hasDatepicker")){
				value = value.replace("/", "");
			webElement.clear();
			}
			if (webDriver.toString().toLowerCase().contains("internet explorer")){
				Actions action = new Actions(webDriver);
				action.sendKeys(webElement, value).build().perform();
			}else {
				webElement.clear();
				webElement.sendKeys(value);
			}			
			
			logger.debug("Elemento: '" + webElement.toString() + "' informado: '" + value + "', via sendkeys");
		} catch (TimeoutException e) {
				msgError = "Erro TimeOut após aguardar " + TIME_OUT + " segundos, o elemento: '" + webElement.toString()
				+ "' não realizado ação 'sendkeys' no valor '" + value + "', tentando por 'action'";
				logger.fatal(msgError, e);
				try{
					Actions action = new Actions(webDriver);		
					action.sendKeys(webElement, value).build().perform();
					logger.debug("Elemento: '" + webElement.toString() + "' informado: '" + value + "', via action");
				}  catch (Exception e1) {
					msgError = "Erro indefinido para o elemento: '" + webElement.toString()
					+ "' durante a execução da ação 'action - sendkeys' no valor '" + value + "'";
					logger.fatal(msgError, e1);
					throw new CommandException(msgError);
				}
		} catch (StaleElementReferenceException e) {
			msgError = "Referencia do elemento: '" + webElement.toString()
			+ "' alterado durante a execução da ação 'send' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}  catch (Exception e) {
			msgError = "Erro indefinido do elemento: '" + webElement.toString()
			+ "' alterado durante a execução da ação 'send' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Simular um click na tecla 'Enter'
	 * 
	 * @param webElement
	 * @throws Exception 
	 */
	public void pressEnter(WebElement webElement) throws Exception {
		try {
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			webElement.sendKeys(Keys.ENTER);
			logger.debug("Elemento: '" + webElement.toString() + "' pressionado tecla 'ENTER'");
		} catch (Exception e) {
			msgError = "Elemento: '" + webElement.toString() + "' não pressionado tecla 'ENTER'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Simular um click na tecla 'Tab'
	 * 
	 * @param webElement
	 * @throws Exception 
	 */
	public void pressTab(WebElement webElement) throws Exception {
		try {
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			webElement.sendKeys(Keys.TAB);
			logger.debug("Elemento: '" + webElement.toString() + "' pressionado tecla 'TAB'");
		} catch (Exception e) {
			msgError = "Elemento: '" + webElement.toString() + "' não pressionado tecla 'TAB'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Simular um click
	 * 
	 * @param webElement
	 * @throws Exception 
	 */
	public void click(WebElement webElement) throws Exception {
		try {
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
			webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
			webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
			
			if (webDriver.toString().toLowerCase().contains("internet explorer")){
				((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", webElement);
			}else {
				webElement.click();
			}
			logger.debug("Elemento: '" + webElement.toString() + "' realizado ação 'click'");
		} catch (Exception e) {
			msgError = "Elemento: '" + webElement.toString() + "' não realizado ação 'click'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Aguardar
	 * 
	 * @param segundos
	 * @throws Exception 
	 */
	public void waitCommand(int segundos) throws Exception {
		try {
			Thread.sleep(1000 * segundos);
			logger.debug("Evento: Aguardar '" + segundos + "s' realizado");
		} catch (IllegalArgumentException e) {
			msgError = "Evento: Aguardar '" + segundos + "s' não realizado";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		} catch (InterruptedException e) {
			msgError = "Evento: Aguardar '" + segundos + "s' não realizado";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * Metodo que navega nos menus dos sistemas
	 * 
	 * @param nome
	 *            dos menus, separados por ":"
	 * @throws Exception
	 */
	public void navigateMenu(String newNavegation) throws Exception {
		String[] navigation = newNavegation.split(":");
		for (int i = 0; i < navigation.length; i++) {
			String xpath = (i == 0) ? ".//ul/li/a/span[contains(@class,'ui-menuitem') and (text()='nomeDoMenu')]" 
					: ".//ul/li/a[contains(@class,'ui-menuitem-link')]//span[contains(@class,'ui-menuitem') and (text()='nomeDoMenu')]";
			xpath = xpath.replace("nomeDoMenu", navigation[i].toString());
			if (navigation.length == i + 1) {
				if (((RemoteWebDriver) webDriver).getCapabilities().getBrowserName().contains("internet explorer")) {
					((JavascriptExecutor) webDriver).executeScript("arguments[0].click();",
							webDriver.findElement(By.xpath(xpath)));
					logger.debug("Click do menu realizado com JavascriptExecutor");
				} else {
					webDriver.findElement(By.xpath(xpath)).click();
					logger.debug("Click do menu realizado com WebDriver");
				}
			} else {
				new Actions(webDriver).moveToElement(webDriver.findElement(By.xpath(xpath))).build().perform();
			}
		}

	}

	/**
	 * Obter valor de 'tooltip'
	 * 
	 * @param webElement
	 * @return
	 * @throws Exception 
	 */
	public String getTooltip(WebElement webElement) throws Exception {
		try {
			new Actions(webDriver).moveToElement(webElement).build().perform();
			String toolTipText = webDriver.findElement(By.cssSelector(".ui-tooltip")).getText();
			logger.info("Evento texto do tooltip realizado no objeto : '" + webElement.toString() + "'");
			return toolTipText;
		} catch (NoSuchElementException e) {
			msgError = "Evento texto do tooltip não realizado para o objeto : '" + webElement.toString() + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}

	/**
	 * A função clica sobre um elemento e aguarda a alteração/apresentação de um
	 * elemento recebido por parametro. Caso o xpath recebido se refira a um
	 * elemento que já está visivel na tela, então aguarda que ocorra alguma
	 * atualização do elemento. Caso o xpath recebido se refira a um elemento
	 * que não está disponível ainda, então aguarda que ele seja apresentado.
	 * 
	 * @param xpathToWait
	 *            XPATH para o elemento que será usado para avaliar se a
	 *            condição foi atingida.
	 * @param friendly
	 *            Nome do elemento que recebe o clique.
	 */
	public void clickAndWait(WebElement elementToClick, String xpathToWait, String friendly) {
		try {
			WebElement e = webDriver.findElement(By.xpath(xpathToWait));
			clickAndWaitStaleness(elementToClick, e, "");
		} catch (NoSuchElementException e) {
			clickAndWaitVisibilty(elementToClick, xpathToWait, "");
		}
	}

	/**
	 * A função clica sobre um elemento e aguarda a alteração/apresentação de um
	 * elemento recebido por parametro.
	 * 
	 * @param elementToWait
	 *            WebElement usado para avaliar se houve alteração
	 * @param friendly
	 *            Nome do elemento que recebe o clique.
	 */
	public void clickAndWait(WebElement elementToClick, WebElement elementToWait, String friendly) {
		clickAndWaitStaleness(elementToClick, elementToWait, friendly);
	}

	/**
	 * Clica sobre o elemento e aguarda até que um dos dados do WebElement
	 * passado por parâmetro seja inválido. A função é util para verificar o
	 * resultado do click de um botão que realiza atualização dos dados.
	 * 
	 * @param values
	 *            WebElement
	 * @param friendly
	 */
	private void clickAndWaitStaleness(WebElement elementToClick, WebElement values, String friendly) {
		final List<WebElement> textFields = values.findElements(By.xpath(".//descendant-or-self::*[text()]"));
		elementToClick.click();
		webDriverWait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver arg0) {
				Iterator<WebElement> iterator = textFields.iterator();
				while (iterator.hasNext()) {
					WebElement e = iterator.next();
					try {
						// Apenas tenta executar um operação sobre o elemento
						// para verifica se
						// a referencia dele ainda é válida
						e.isEnabled();
					} catch (StaleElementReferenceException exc) {
						return true;
					}
				}
				return false;
			}
		});
	}

	/**
	 * Clica sobre o elemento e aguarda até que o XPATH recebido esteja visivel.
	 * 
	 * @param xpath
	 *            Xpath para o elemento que espera que seja visivel
	 * @param friendly
	 *            Nome do botão que será clicado.
	 */
	private void clickAndWaitVisibilty(WebElement webElement, String xpath, String friendly) {
		webElement.click();
		webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	/**
	 * Método para expandir uma linha da tabela. Recebe como parametro um
	 * WebElement com a tag TR que deseja expandir, e finaliza apenas quando ela
	 * estiver visível.
	 * 
	 * @param row
	 *            Um WebElement com a tr que deseja expandir
	 * 
	 */
	public void expandRow(WebElement row) {
		clickAndWait(row.findElement(By.xpath(".//div[contains(@class, 'ui-row-toggler') ]")),
				"//tr[contains (@class, 'ui-expanded-row-content')]", "Botão de Expandir");
	}

	/**
	 * Auto complete.
	 *
	 * @param value
	 *            the value
	 * @throws Exception
	 *             the exception
	 */
	public void autoComplete(WebElement webElement, String value) throws Exception {
		String attribute = null;
		webDriverWait.until(ExpectedConditions
				.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));
		webElement = webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
		attribute = webElement.getAttribute("class");
		if (attribute.contains("hasDatepicker"))
			value = value.replace("/", "");

		try {
			webElement.clear();
			char[] val = value.toCharArray();
			for (char c : val) {
				webElement.sendKeys(String.valueOf(c));
				waitCommand(2);
			}
			logger.debug("Elemento: '" + webElement.toString() + "' informado: '" + value + "'");
		} catch (Exception e) {
			logger.fatal("Elemento: '" + webElement.toString() + "' não informado: '" + value + "'", e);
			throw new CommandException(e);
		}
	}

	/**
	 * Frame
	 * 
	 * @param name
	 */
	public void setFrame(String name) {
		List<WebElement> frames = webDriver.findElements(By.tagName("Frame"));
		for (WebElement frame : frames) {
			logger.info("Frame localizado " + frame.getAttribute("name"));
			if (frame.getAttribute("name").contains(name)) {
				logger.info("Alterado para o frame " + frame.getAttribute("name"));
				webDriver.switchTo().frame(frame);
				return;
			}
		}
		String msgError = "Nenhum frame com o nome '" + name + "' foi localizado.";
		logger.info(msgError);
	}

	/**
	 * Focus
	 * 
	 * @param webElement
	 */
	public void focus(WebElement webElement) {
		((JavascriptExecutor) webDriver).executeScript(FOCUS_SCRIPT, webElement);
	}
	
	/**
	 * Método para realizar click no botão 'OK' do Alert do javascript,
	 *  aguardando o tempo em segundos especificado pelo paramentro TimeOut.
	 * @param TimeOut 
	 * @throws Exception 
	 */
	public void clickButtonAlert(int TimeOut) throws Exception{
		try{
			new WebDriverWait(webDriver, TimeOut).until(ExpectedConditions.alertIsPresent());
	        Alert alert = webDriver.switchTo().alert();
	        alert.accept();
		}catch(TimeoutException e){
			logger.error("Alert não localizado. Timeout = " + TIME_OUT);
		}
		catch(Exception e1){
			throw new CommandException(e1);
		}
	}
	
	/**
	 * Método para realizar click no botão 'OK' do Alert do Javascript.
	 * TimeOut padrão de 30 segundos.
	 * @throws Exception 
	 */
	public void clickButtonAlert() throws Exception{
		clickButtonAlert(30);
	}
	
	/**
	 * Enviar valor para os campos de 'input' do site atual.
	 * 
	 * @param webElement
	 * @param value
	 * @throws Exception 
	 */
	public void sendSiteQasNew(WebElement webElement, String value) throws Exception {
		
		try {
			webElement = webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
			webDriverWait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.xpath("//div[contains(@id,'statusDialogWaiting')]")));

			webElement.clear();
			webElement.sendKeys(value);
			logger.debug("Elemento: '" + webElement.toString() + "' informado: '" + value + "', via sendkeys");
		} catch (TimeoutException e) {
				msgError = "Erro TimeOut após aguardar " + TIME_OUT + " segundos, o elemento: '" + webElement.toString()
				+ "' não realizado ação 'sendkeys' no valor '" + value + "', tentando por 'action'";
				logger.fatal(msgError, e);
				try{
					Actions action = new Actions(webDriver);		
					action.sendKeys(webElement, value).build().perform();
					logger.debug("Elemento: '" + webElement.toString() + "' informado: '" + value + "', via action");
				}  catch (Exception e1) {
					msgError = "Erro indefinido para o elemento: '" + webElement.toString()
					+ "' durante a execução da ação 'action - sendkeys' no valor '" + value + "'";
					logger.fatal(msgError, e1);
					throw new CommandException(msgError);
				}
		} catch (StaleElementReferenceException e) {
			msgError = "Referencia do elemento: '" + webElement.toString()
			+ "' alterado durante a execução da ação 'send' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}  catch (Exception e) {
			msgError = "Erro indefinido do elemento: '" + webElement.toString()
			+ "' alterado durante a execução da ação 'send' no valor '" + value + "'";
			logger.fatal(msgError, e);
			throw new CommandException(msgError);
		}
	}
}