package br.com.treinamento.devops.selenium.browser;

public enum BrowserType {
	
	internetExplorer, firefox, chrome, edge

}
