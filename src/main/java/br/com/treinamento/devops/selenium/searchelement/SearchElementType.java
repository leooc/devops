package br.com.treinamento.devops.selenium.searchelement;

import org.openqa.selenium.WebDriver;

public enum SearchElementType {

	GENERIC {
		@Override
		public SearchElement getSearchElement(WebDriver webDriver) {
			return new SearchElementGeneric(webDriver);
		}
	},
	OPCAOUM {
		@Override
		public SearchElement getSearchElement(WebDriver webDriver) {   
			return new SearchElementGeneric(webDriver);
		}
	},
	OPCAODOIS {
		@Override
		public SearchElement getSearchElement(WebDriver webDriver) {
			return new SearchElementGeneric(webDriver);
		}
	};
	
	/**
	 * Obter SearchElement
	 * @param webDriver
	 * @return
	 */
	public abstract SearchElement getSearchElement (WebDriver webDriver);
	
}