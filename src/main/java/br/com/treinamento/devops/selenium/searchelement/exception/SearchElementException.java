package br.com.treinamento.devops.selenium.searchelement.exception;

public class SearchElementException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SearchElementException(String mensagem) {
		super(mensagem);
	}
	
	public SearchElementException(Exception e) {
		super(e);
	}
	
	public SearchElementException(String mensagem, Exception e) {
		super(mensagem, e);
	}

}
