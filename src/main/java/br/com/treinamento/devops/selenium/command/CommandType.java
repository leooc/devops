package br.com.treinamento.devops.selenium.command;

import org.openqa.selenium.WebDriver;

public enum CommandType {

	GENERIC{
		@Override
		public Command getCommand(WebDriver webDriver){	
			return new CommandGeneric(webDriver); 
		}
	},
	OPCAOUM{
		@Override
		public Command getCommand(WebDriver webDriver){	
			return new CommandGeneric(webDriver); 
		}
	},
	OPCAODOIS{
		@Override
		public Command getCommand(WebDriver webDriver){
			return new CommandGeneric(webDriver); 
		}		
	};
	
	/**
	 * Obter command
	 * @param webDriver
	 * @return
	 */
	public abstract Command getCommand(WebDriver webDriver);
}
