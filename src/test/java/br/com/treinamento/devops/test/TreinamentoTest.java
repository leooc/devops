package br.com.treinamento.devops.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/treinamentoUm.feature",  
glue = "br.com.treinamento", monochrome = true)
public class TreinamentoTest {
	
} 