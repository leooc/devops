﻿#language: pt
#Encoding: ISO-8859-1
#Version: 1.0
  
Funcionalidade: Testar as operacoes basicas java/selenium/cucumber
      
Contexto: Cuida do webDriver e sua instancia
  Dado que o navegador esteja aberto
    | IE |
    | Firefox | 
    | Chrome | 
            
@before
@after
Delineacao do Cenario: Teste
E entro no "<site>" da cielo
E preencho os campos do formulario
E realizo o click para continuar
Entao verifico se apareceu o label "Informações adicionais"

Exemplos:
| site |
| www.cielo.com.br |

@before
@after
Delineacao do Cenario: TesteDois
E entro no "<site>" da cielo
E preencho os campos do formulario
E realizo o click para continuar
Entao verifico se apareceu o label "Informações adicionais"

Exemplos:
| site |
| www.cielo.com.br |
  

   
    